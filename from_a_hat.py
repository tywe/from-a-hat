from collections import defaultdict
from copy import deepcopy
from flask import Flask, render_template, request
from flask_toastr import Toastr
import pickle
from random import choice

flask_app = Flask(__name__)
toastr = Toastr(flask_app)

def write_file(filename, items, mode="wb+"):
    with open(filename, mode) as f:
        pickle.dump(items, f)


def read_file(filename):
    with open(filename, "rb") as f:
        items = pickle.load(f)
    return items


@flask_app.route("/")
def index():
    return render_template("index.html")


@flask_app.route("/who", methods=["POST"])
def names():
    assignments = request.form["assignments"].splitlines()
    assignments = list(filter(None, assignments)) # remove blanks
    write_file("generated/assignments.pickle", assignments)
    return render_template("who.html", number_of_assignments=len(assignments))


@flask_app.route("/ready", methods=["POST"])
def ready():
    names = request.form["names"].splitlines()
    names = list(filter(None, names)) # remove blanks
    write_file("generated/names.pickle", names)
    # Clear out old matches
    write_file("generated/matches.pickle", dict())
    return render_template("ready.html")


@flask_app.route("/hat", methods=["POST"])
def hat():
    assignments = read_file("generated/assignments.pickle")
    names = read_file("generated/names.pickle")
    previous_matches = read_file("generated/matches.pickle")

    if not assignments:
        # hat is empty
        return render_template("result.html", matches=previous_matches)

    assignment = choice(assignments)
    name = choice(names)

    new_matches = deepcopy(previous_matches)
    new_matches[assignment] = name

    assignments.remove(assignment)
    names.remove(name)

    write_file("generated/assignments.pickle", assignments)
    write_file("generated/names.pickle", names)
    write_file("generated/matches.pickle", new_matches)

    return render_template("hat.html", assignment=assignment, name=name, matches=previous_matches, remaining_assignments=len(assignments))